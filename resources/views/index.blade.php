<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
      <link rel="stylesheet" href="assets/css/style.css">
      <title>Home</title>
   </head>
   <body class="index">
     
      
      <!-- For mobile -->
      <header class="header header-transparent">
         <div class="header-middle">
            <div class="container">
               <div class="header-left">
                  <button class="mobile-menu-toggler" type="button">
                  <img src="assets/img/mob-menu.png">
                  </button>
                  <select class="currency-selector">
                     <option selected>CDMX</option>
                     <option >ABC</option>
                  </select>
               </div>
               <!-- End .header-left -->
			       <a href="index.html" class="logo">
                  <img src="assets/img/logo.png" alt="Logo">
                  </a>
               <div class="header-right">
                     <div class="header-user acco">
                        <img src="assets/img/mob-user.png">
                     </div>
                  <div class="dropdown cart-dropdown">
                     <a href="#" class="dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-display="static">
                        <img src="assets/img/mob-cart.png">
                        <!--<span class="cart-count">2</span>-->
                     </a>
                  </div>
                  <!-- End .dropdown -->
                  <div class="header-search">
                     <a href="#" class="search-toggle" role="button"><img src="assets/img/mob-search.png"></a>
                     <!-- <form action="#" method="get">
                        <div class="header-search-wrapper">
                            <input type="search" class="form-control" name="q" id="q" placeholder="I'm searching for..." required="">
                           
                            <button class="btn" type="submit"><i class="icon-search-3"></i></button>
                        </div>
                        </form>-->
                  </div>
               </div>
               <!-- End .header-right -->
            </div>
            <!-- End .container-fluid -->
         </div>
         <!-- End .header-middle -->
         <div class="top-cate">
            <button><span>Más vendido</span></button>
            <button><Span>Hogar y Cocina</span></a>
               <button><span>Ac</span></button>
               <button><span>Relojes</span></button>
               <button><span>Más vendido</span></button>
               <button><Span>Hogar y Cocina</span></button>
               <button><span>Relojes</span></button>
            </div>
      </header>
      <!-- End .header -->
      <!-- end --> 
      
      <!-- slider -->
      <section class="mobile slider ">
      <div id="demo" class="carousel slide " data-ride="carousel" >
         <ul class="carousel-indicators">
            <li data-target="#demo" data-slide-to="0" class="active"></li>
            <li data-target="#demo" data-slide-to="1"></li>
            <li data-target="#demo" data-slide-to="2"></li>
         </ul>
         <div class="carousel-inner">
            <div class="carousel-item active">
               <img src="assets/img/b2.png" alt="banner">
               <!-- <div class="carousel-caption">
                  <h3>Los Angeles</h3>
                  <p>We had such a great time in LA!</p>
                  </div>-->   
            </div>
            <div class="carousel-item">
               <img src="assets/img/b2.png" alt="banner">
               <!--<div class="carousel-caption">
                  <h3>Chicago</h3>
                  <p>Thank you, Chicago!</p>
                  </div> -->  
            </div>
            <div class="carousel-item">
               <img src="assets/img/b2.png" alt="banner">
               <!--<div class="carousel-caption">
                  <h3>New York</h3>
                  <p>We love the Big Apple!</p>
                  </div>-->   
            </div>
         </div>
         <a class="carousel-control-prev" href="#demo" data-slide="prev">
         <span class="carousel-control-prev-icon"></span>
         </a>
         <a class="carousel-control-next" href="#demo" data-slide="next">
         <span class="carousel-control-next-icon"></span>
         </a>
      </div>
      
   </section>
      <!-- slider -->
      <!-- sidebar  -->
      <!-- sidebar  -->
  <!-- sidebar  -->
     
      </section>
      <!-- popup -->
         <div class="overlayer" style="display: none;">
            <div class="popup">
               <div class="login">
                  <section >
                     <div class="header-middle">
                        <div class="container">
                           <div class="row align-items-center">
                              <div class="col-4">
                                 <div class="header-left">
                                    <i class='fas fa-times ml-2 ' id="hide" style="font-size: 20px;"></i>
                                 </div>
                              </div>
                              <div class="col-8">
                                 <a href="index.html" class="logo">
                                    <img src="assets/img/logo.png" alt="Logo">
                                    </a>
                              </div>
                           </div>
                           
                           <!-- End .header-left -->
                           
                           <!-- End .header-right -->
                        </div>
                        <!-- End .container-fluid -->
                     </div>
                    <!-- End .header-middle -->
                  </section>
                  <div class="container-fluid mt-3">
                     
                     <div class="row">
                        <div class="col-12">
                            <ul class="nav nav-tabs tab_1">
                                <li class="active"><a data-toggle="tab" href="#home">INGRESAR</a></li>
                                <li><a data-toggle="tab" href="#menu1">REGISTRARSE</a></li>
                              </ul>
            
                              <div class="tab-content">
                                <div id="home" class="tab-pane in active">
                                    <form class="form">
                                        <div class="row">
                                            <div class="col-12">
                                                <input type="text" class="form-control mb-2 mr-sm-2" id="inlineFormInputName2" placeholder="Correo electronico">
                                            </div>
                                            <div class="col-12">
                                                <div class="input-group mb-2 mr-sm-2">
                                                <input type="text" class="form-control" id="inlineFormInputGroupUsername2" placeholder="Contrasena">
                                                </div>
                                            </div>
                                            
                                        </div>
                                        <div class="row my-3">
                                            <div class="col-12 text-center">
                                                <a href="#" class="orange">Iniciar sesion</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div id="menu1" class="tab-pane">
                                    <form class="form">
                                        <div class="row">
                                            <div class="col-12">
                                                <input type="text" class="form-control mb-2 mr-sm-2" id="inlineFormInputName2" placeholder="Correo electronico">
                                            </div>
                                            <div class="col-12">
                                                <div class="input-group mb-2 mr-sm-2">
                                                <input type="text" class="form-control" id="inlineFormInputGroupUsername2" placeholder="Contrasena">
                                                </div>
                                            </div>
                                            
                                        </div>
                                        <div class="row my-3">
                                            <div class="col-12 text-center">
                                                <a href="#" class="orange">REGISTRARSE</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                
                              </div>
                            
                              <div class="row my-3">
                                <div class="col-12 text-center line">
                                    <p><span class="pull-left"></span>o<span class="pull-right"></span></p>
                                </div>
                            </div>
                            <div class="row my-3">
                                <div class="col-12 text-center">
                                    <a href="#" class="facebook"><i class="fab fa-facebook-square"></i> Iniciar sesion con Facebook</a>
                                </div>
                            </div>
                            <div class="row my-3">
                                <div class="col-12 text-center">
                                    <a href="#" class="gmail">
                                        <img src="assets/img/gmail.png" width="30px" style="margin-top: -6px;">
                                        <span>Iniciar sesion con Gmail</span>
                                    </a>
                                </div>
                            </div>
                            <div class="row my-3">
                                <div class="col-12 text-center grey">
                                    <p>AI inciar sesion usted acepta <br> los <span class="red"><a href="#">treminos y condiciones y politicas de privacidad</a></span> </p>
                                </div>
                            </div>
                        </div>
                     </div>
                  </div>
               </div>
               
            </div>
         </div>
      <!-- popup -->
      
      <section class="container-fluid item-product">
         <div class="row row-sm">
         <div class="col-6 ">
            <div class="product-default">
               <figure>
                  <a href="{{ route('product.list') }}">
                  <img src="assets/img/p1.jpg">
                  </a>
               </figure>
               <div class="product-details">
                  <h2 class="product-title">
                     <a href="{{ route('product.list') }}">Descuentos exclusivos! Solo para app</a>
                  </h2>
                  <div class="price-box">
                     <span class="product-price"><i>$39</i> MXN</span>
                  </div>
                  <!-- End .price-box -->
                  <div class="category-wrap">
                     <div class="category-list">
                        <a href="category.html" class="product-category">recibelo</a>
                     </div>
                  </div>
               </div>
               <!-- End .product-details -->
            </div>
         </div>
         <div class="col-6 ">
            <div class="product-default">
               <figure>
                  <a href="{{ route('product.list') }}">
                  <img src="assets/img/p2.jpg">
                  </a>
               </figure>
               <div class="product-details">
                  <h2 class="product-title">
                     <a href="{{ route('product.list') }}">Descuentos exclusivos! Solo para app</a>
                  </h2>
                  <div class="price-box">
                     <span class="product-price"><i>$39</i> MXN</span>
                  </div>
                  <!-- End .price-box -->
                  <div class="category-wrap">
                     <div class="category-list">
                        <a href="category.html" class="product-category">recibelo</a>
                     </div>
                  </div>
               </div>
               <!-- End .product-details -->
            </div>
         </div>
         <div class="col-6">
            <div class="product-default">
               <figure>
                  <a href="{{ route('product.list') }}">
                  <img src="assets/img/p2.jpg">
                  </a>
               </figure>
               <div class="product-details">
                  <h2 class="product-title">
                     <a href="{{ route('product.list') }}">Descuentos exclusivos! Solo para app</a>
                  </h2>
                  <div class="price-box">
                     <span class="product-price"><i>$39</i> MXN</span>
                  </div>
                  <!-- End .price-box -->
                  <div class="category-wrap">
                     <div class="category-list">
                        <a href="category.html" class="product-category">recibelo</a>
                     </div>
                  </div>
               </div>
               <!-- End .product-details -->
            </div>
         </div>
         <div class="col-6">
            <div class="product-default">
               <figure>
                  <a href="{{ route('product.list') }}">
                  <img src="assets/img/p1.jpg">
                  </a>
               </figure>
               <div class="product-details">
                  <h2 class="product-title">
                     <a href="{{ route('product.list') }}">Descuentos exclusivos! Solo para app</a>
                  </h2>
                  <div class="price-box">
                     <span class="product-price"><i>$39</i> MXN</span>
                  </div>
                  <!-- End .price-box -->
                  <div class="category-wrap">
                     <div class="category-list">
                        <a href="category.html" class="product-category">recibelo</a>
                     </div>
                  </div>
               </div>
               <!-- End .product-details -->
            </div>
         </div>
      </section>
      <!-- footer  -->
      <footer>
         <div class="container-fluid">
            <div class="row">
               <div class="col-lg-6 col-12">
                  <p>Rasterio de Predidos y dudas sobre nuestros articulos</p>
                  <p>Mensaje de WhatsApp: 55 8732<br>2760</p>
               </div>
               <div class="col-lg-6 col-12">
                  <img src="assets/img/app-store.png" width="150px">
                  <img src="assets/img/google.png" width="150px">
               </div>
            </div>
            <div class="row">
               <div class="col-lg-6"></div>
               <div class="col-lg-6">
                  <p>Menu Inferior</p>
                  <ul>
                     <li><a href="#">Busquesda</a></li>
                     <li><a href="#">Preguntas Frecuentes</a></li>
                     <li><a href="#">Politica de privacidad</a></li>
                     <li><a href="#">Envio Y devoluciones</a></li>
                     <li><a href="#">Quiesnes somos</a></li>
                  </ul>
               </div>
            </div>
            <div class="row">
               <div class="col-12">
                  <p>Atencion Al Cliente (Cambios)</p>
                  <p>Mensaje de WhatsApp: 55 8029<br>8963</p>
               </div>
            </div>
         </div>
      </footer>
      <!-- footer  -->
      <div class="app-noti">
         <a><i class="fa fa-times" aria-hidden="true"></i></a><span><img src="assets/img/app-logo.png"></span>
         <div class="">
            <h4>Gangabox App</h4>
            <span class="sml">Descuentos exclusivos! Solo para app</span><span><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i> (31,046)</span>
         </div>
         <span><a href="" class="app-btn">descargar</a></span>
      </div>
      <div class="mobile-menu-overlay"></div>
      <!-- End .mobil-menu-overlay -->
      <div class="mobile-menu-container">
         <div class="mobile-menu-wrapper">
            <div class="menu-top"> <span><img src="assets/img/logo.png"></span><span class="mobile-menu-close"><img src="assets/img/close.png"></span></div>
            <nav class="mobile-nav">
               <ul class="mobile-menu">
                  <li class="dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"> <span class="caret">Categorías</span></a>
                     <ul class="dropdown-menu">
                        <li><a href="listing.html">submenu</a></li>
                        <li><a href="listing.html">submenu</a></li>
                        <li><a href="listing.html">submenu</a></li>
                     </ul>
                  </li>
                  <li>
                     <a href="listing.html">Más vendido</a>
                  </li>
                  <li>
                     <a href="listing.html">Liquidacíon</a>
                  </li>
                  <li>
                     <a href="listing.html">Contactanos</a>
                  </li>
               </ul>
            </nav>
            <!-- End .mobile-nav -->
         </div>
         <!-- End .mobile-menu-wrapper -->
      </div>
      <!-- End .mobile-menu-container -->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
      <script>
         
      </script>
      <script>
         
      </script>
      <script src='https://kit.fontawesome.com/a076d05399.js'></script>
      <script src="assets/js/number.js"></script>
   </body>
</html>