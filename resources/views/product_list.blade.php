<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <script src='https://kit.fontawesome.com/a076d05399.js'></script>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
      <link rel="stylesheet" href="assets/css/style.css">
      <title>Gangabox | Product_list</title>
   </head>
   <body>
      

      <!-- mobile view  -->
      <div class="mobile_view">
          <div id="myHeader" class="category_top">
        <div class="category_title">
           <div><a class="back" href="index.html"><img src="assets/img/back.png"></a></div>
           <div>
              <h2>Almacenamiiento y organizacion</h2>
              <span class="total product">11 Productors</span>
           </div>
           <div>
              <div class="dropdown cart-dropdown">
                 <a href="#" class="dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-display="static">
                    <img src="assets/img/mob-cart.png">
                    <!--<span class="cart-count">2</span>-->
                 </a>
              </div>
              <!-- End .dropdown -->
           </div>
        </div>
        <section class="filter_Sec">
           <div class="select-custom">
              <select name="orderby" class="form-control">
                 <option value="menu_order" selected="selected">ordenar</option>
                 <option value="popularity">1</option>
                 <option value="rating">2</option>
                 <option value="date">3</option>
                 <option value="price">4</option>
                 <option value="price-desc">5</option>
              </select>
           </div>
           <div class="filter">
              <span>filtro <img src="assets/img/filter.png"></span>
           </div>
        </section>
            </div>
            <div class="layered-filter-wrapper">
            <div class="mobile-menu-container">
                <div class="filter-menu">
                    <div class="menu-top"> <span>FILTER</span><span class="close"><img src="assets/img/close.png"></span></div>
                    <nav class="mobile-nav">
                        <ul class="mobile-menu">
                        <li class="active"><a href="index.html">ambietador </a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"> <span class="caret">bleach</span></a>
                            <ul class="dropdown-menu">
                                <li><a href="">submenu</a></li>
                                <li><a href="">submenu</a></li>
                                <li><a href="">submenu</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="">bolsas de basura</a>
                        </li>
                        <li>
                            <a href="#">control de plagas</a>
                        </li>
                        <li>
                            <a href="#">control de plagas</a>
                        </li>
                        <li>
                            <a href="#">cuidado de ropa</a>
                        </li>
                        <li>
                            <a href="#">dientes</a>
                        </li>
                        <li>
                            <a href="#">herramientas de limpieza</a>
                        </li>
                        
                        <li class="active"><a href="index.html">ambietador </a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"> <span class="caret">bleach</span></a>
                            <ul class="dropdown-menu">
                                <li><a href="">submenu</a></li>
                                <li><a href="">submenu</a></li>
                                <li><a href="">submenu</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="">bolsas de basura</a>
                        </li>
                        <li>
                            <a href="#">control de plagas</a>
                        </li>
                        <li>
                            <a href="#">control de plagas</a>
                        </li>
                        <li>
                            <a href="#">cuidado de ropa</a>
                        </li>
                        <li>
                            <a href="#">dientes</a>
                        </li>
                        <li>
                            <a href="#">herramientas de limpieza</a>
                        </li>
                            <li>
                            <a href="">bolsas de basura</a>
                        </li>
                        <li>
                            <a href="#">control de plagas</a>
                        </li>
                        <li>
                            <a href="#">control de plagas</a>
                        </li>
                        <li>
                            <a href="#">cuidado de ropa</a>
                        </li>
                        <li>
                            <a href="#">dientes</a>
                        </li>
                        </ul>
                    </nav>
                    <!-- End .mobile-nav -->
                </div>
                <!-- End .filter-menu -->
                <div class="select_btn"><a href="#">Seleccionar</a></div>
            </div>
            <!-- End .mobile-menu-container -->
            </div>
            <section class="container-fluid item-product" style="display:block;">
            <div class="row row-sm">
            <div class="col-6 col-md-4 col-lg-3 col-xl-2">
                <div class="product-default">
                    <figure>
                        <a href="{{ route('product.description') }}">
                        <img src="assets/img/p1.jpg">
                        </a>
                    </figure>
                    <div class="product-details">
                        <h2 class="product-title">
                        <a href="{{ route('product.description') }}">Descuentos exclusivos! Solo para app</a>
                        </h2>
                        <div class="price-box">
                        <span class="product-price"><i>$39</i> MXN</span>
                        </div>
                        <!-- End .price-box -->
                        <div class="category-wrap">
                        <div class="category-list">
                            <a href="category.html" class="product-category">recibelo</a>
                        </div>
                        </div>
                    </div>
                    <!-- End .product-details -->
                </div>
            </div>
            <div class="col-6 col-md-4 col-lg-3 col-xl-2">
                <div class="product-default">
                    <figure>
                        <a href="{{ route('product.description') }}">
                        <img src="assets/img/p2.jpg">
                        </a>
                    </figure>
                    <div class="product-details">
                        <h2 class="product-title">
                        <a href="{{ route('product.description') }}">Descuentos exclusivos! Solo para app</a>
                        </h2>
                        <div class="price-box">
                        <span class="product-price"><i>$39</i> MXN</span>
                        </div>
                        <!-- End .price-box -->
                        <div class="category-wrap">
                        <div class="category-list">
                            <a href="category.html" class="product-category">recibelo</a>
                        </div>
                        </div>
                    </div>
                    <!-- End .product-details -->
                </div>
            </div>
            <div class="col-6 col-md-4 col-lg-3 col-xl-2">
                <div class="product-default">
                    <figure>
                        <a href="{{ route('product.description') }}">
                        <img src="assets/img/p2.jpg">
                        </a>
                    </figure>
                    <div class="product-details">
                        <h2 class="product-title">
                        <a href="{{ route('product.description') }}">Descuentos exclusivos! Solo para app</a>
                        </h2>
                        <div class="price-box">
                        <span class="product-price"><i>$39</i> MXN</span>
                        </div>
                        <!-- End .price-box -->
                        <div class="category-wrap">
                        <div class="category-list">
                            <a href="category.html" class="product-category">recibelo</a>
                        </div>
                        </div>
                    </div>
                    <!-- End .product-details -->
                </div>
            </div>
            <div class="col-6 col-md-4 col-lg-3 col-xl-2">
                <div class="product-default">
                    <figure>
                        <a href="{{ route('product.description') }}">
                        <img src="assets/img/p1.jpg">
                        </a>
                    </figure>
                    <div class="product-details">
                        <h2 class="product-title">
                        <a href="{{ route('product.description') }}">Descuentos exclusivos! Solo para app</a>
                        </h2>
                        <div class="price-box">
                        <span class="product-price"><i>$39</i> MXN</span>
                        </div>
                        <!-- End .price-box -->
                        <div class="category-wrap">
                        <div class="category-list">
                            <a href="category.html" class="product-category">recibelo</a>
                        </div>
                        </div>
                    </div>
                    <!-- End .product-details -->
                </div>
            </div>
            <div class="col-6 col-md-4 col-lg-3 col-xl-2">
                <div class="product-default">
                    <figure>
                        <a href="{{ route('product.description') }}">
                        <img src="assets/img/p1.jpg">
                        </a>
                    </figure>
                    <div class="product-details">
                        <h2 class="product-title">
                        <a href="{{ route('product.description') }}">Descuentos exclusivos! Solo para app</a>
                        </h2>
                        <div class="price-box">
                        <span class="product-price"><i>$39</i> MXN</span>
                        </div>
                        <!-- End .price-box -->
                        <div class="category-wrap">
                        <div class="category-list">
                            <a href="category.html" class="product-category">recibelo</a>
                        </div>
                        </div>
                    </div>
                    <!-- End .product-details -->
                </div>
            </div>
            <div class="col-6 col-md-4 col-lg-3 col-xl-2">
                <div class="product-default">
                    <figure>
                        <a href="{{ route('product.description') }}">
                        <img src="assets/img/p1.jpg">
                        </a>
                    </figure>
                    <div class="product-details">
                        <h2 class="product-title">
                        <a href="{{ route('product.description') }}">Descuentos exclusivos! Solo para app</a>
                        </h2>
                        <div class="price-box">
                        <span class="product-price"><i>$39</i> MXN</span>
                        </div>
                        <!-- End .price-box -->
                        <div class="category-wrap">
                        <div class="category-list">
                            <a href="category.html" class="product-category">recibelo</a>
                        </div>
                        </div>
                    </div>
                    <!-- End .product-details -->
                </div>
            </div>
            <div class="col-6 col-md-4 col-lg-3 col-xl-2">
                <div class="product-default">
                    <figure>
                        <a href="{{ route('product.description') }}">
                        <img src="assets/img/p1.jpg">
                        </a>
                    </figure>
                    <div class="product-details">
                        <h2 class="product-title">
                        <a href="{{ route('product.description') }}">Descuentos exclusivos! Solo para app</a>
                        </h2>
                        <div class="price-box">
                        <span class="product-price"><i>$39</i> MXN</span>
                        </div>
                        <!-- End .price-box -->
                        <div class="category-wrap">
                        <div class="category-list">
                            <a href="category.html" class="product-category">recibelo</a>
                        </div>
                        </div>
                    </div>
                    <!-- End .product-details -->
                </div>
            </div>
            <div class="col-6 col-md-4 col-lg-3 col-xl-2">
                <div class="product-default">
                    <figure>
                        <a href="{{ route('product.description') }}">
                        <img src="assets/img/p1.jpg">
                        </a>
                    </figure>
                    <div class="product-details">
                        <h2 class="product-title">
                        <a href="{{ route('product.description') }}">Descuentos exclusivos! Solo para app</a>
                        </h2>
                        <div class="price-box">
                        <span class="product-price"><i>$39</i> MXN</span>
                        </div>
                        <!-- End .price-box -->
                        <div class="category-wrap">
                        <div class="category-list">
                            <a href="category.html" class="product-category">recibelo</a>
                        </div>
                        </div>
                    </div>
                    <!-- End .product-details -->
                </div>
            </div>
            </section>
            <div class="filter-overlay"></div>
      </div>
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
        <script src="assets/js/number.js"></script>
        <script>
           $('.filter span').on('click', function (e) {
           $('body').toggleClass('mmenu-active');
           $(this).toggleClass('active');
           e.preventDefault();
           });
           
           $('.filter-overlay, .close').on('click', function (e) {
           $('body').removeClass('mmenu-active');
           e.preventDefault();
           });
        </script>
        <script>
            window.onscroll = function() {myFunction()};
            
            var header = document.getElementById("myHeader");
            var sticky = header.offsetTop;
            
            function myFunction() {
                if (window.pageYOffset > sticky) {
                header.classList.add("sticky");
                } else {
                header.classList.remove("sticky");
                }
            }
        </script>

      <!-- mobile view  -->
   </body>
</html>